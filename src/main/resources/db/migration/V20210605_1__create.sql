drop table if exists auth;
create table auth(
                     id serial,
                     login varchar(32),
                     password varchar(32),
                     last_login_timestamp bigint,
                     user_id bigint,
                     token varchar(255)
);