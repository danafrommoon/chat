ALTER TABLE message
    ADD isRead boolean;
ALTER TABLE message
    ADD isDelivered boolean;
ALTER TABLE message
    ADD readTimestamp bigint;

ALTER TABLE message
    ADD message_type varchar(50);