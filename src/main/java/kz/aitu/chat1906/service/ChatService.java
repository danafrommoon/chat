package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Chat;
import kz.aitu.chat1906.repository.ChatRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ChatService {
    private final ChatRepository chatRepository;

    public void add(Chat chat) {
        chatRepository.save(chat);
    }

    public List<Chat> getAll() {
        return chatRepository.findAll();
    }

    public void update(Chat chat) {
        chatRepository.save(chat);
    }

    public void delete(Chat chat) {
        chatRepository.delete(chat);
    }
}
