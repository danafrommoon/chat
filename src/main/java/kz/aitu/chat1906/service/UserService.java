package kz.aitu.chat1906.service;

import kz.aitu.chat1906.model.Users;
import kz.aitu.chat1906.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public void add(Users user) {
        userRepository.save(user);
    }

    public List<Users> getAll() {
        return userRepository.findAll();
    }

    public void update(Users user) {
        userRepository.save(user);
    }

    public void delete(Users user) {
        userRepository.delete(user);
    }
    public Users getById(Long userId){
        return userRepository.getUserById(userId);
    }
}
