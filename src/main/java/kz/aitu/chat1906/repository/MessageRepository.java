package kz.aitu.chat1906.repository;

import kz.aitu.chat1906.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message,Long> {
    List<Message> findMessagesByChatId(Long chatId);
    @Query(value = "SELECT * FROM message " +
            "where chat_id = ?1" +
            "order by id DESC" +
            "limit 10;", nativeQuery=true)
    List<Message> findLastMessagesByChatId(Long chatId);
    @Query(value = "SELECT * FROM message " +
            "where user_id = ?1" +
            "order by id DESC" +
            "limit 10;",nativeQuery=true)
    List<Message> findLastMessagesByUsertId(Long userId);

    @Query(value = "SELECT * FROM message " +
            "where chat_id = ?1 and user_id <> ?2 and isRead = false " +
            "order by id DESC;",nativeQuery=true)
    List<Message> findNotReadMessages(Long chatid, Long userId);

    @Query(value = "SELECT * FROM message " +
            "where chat_id = ?1 and user_id <> ?2 and isDelivered = false " +
            "order by id DESC;",nativeQuery=true)
    List<Message> findNotDeliveredMessages(Long chatid, Long userid);

    List<Message> findByChatIdAndUserIdNot(Long chatid, Long userid);
}
