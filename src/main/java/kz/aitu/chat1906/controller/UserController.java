package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Auth;
import kz.aitu.chat1906.model.Users;
import kz.aitu.chat1906.service.AuthService;
import kz.aitu.chat1906.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/user")
public class UserController {

    private final AuthService authService;
    private final UserService userService;

    @GetMapping("/allAuth")
    public ResponseEntity<?> getAllAuth() {
        return ResponseEntity.ok(authService.getAll());
    }

    @GetMapping
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(userService.getAll());
    }

    @PostMapping
    public ResponseEntity<?> register(@RequestBody Users user, @RequestBody Auth auth) {
        authService.login(auth);
        userService.add(user);
        return ResponseEntity.ok("User successfully added");
    }

    @PutMapping
    public ResponseEntity<?> login(@RequestBody Users user, @RequestBody Auth auth) {
        authService.update(auth);
        userService.update(user);
        return ResponseEntity.ok(user);
    }
    @DeleteMapping
    public ResponseEntity<?> delete(@RequestBody Users user, @RequestBody Auth auth) {
        authService.delete(auth);
        userService.delete(user);
        return ResponseEntity.ok("User is deleted");
    }
}
