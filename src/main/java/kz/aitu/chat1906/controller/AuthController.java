package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Auth;
import kz.aitu.chat1906.model.Chat;
import kz.aitu.chat1906.model.Message;
import kz.aitu.chat1906.model.Users;
import kz.aitu.chat1906.service.AuthService;
import kz.aitu.chat1906.service.ChatService;
import kz.aitu.chat1906.service.MessageService;
import kz.aitu.chat1906.service.ParticipantService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/login")
public class AuthController {

    private ChatService chatService;
    private MessageService messageService;
    private AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestParam String login, @RequestParam String password) {
        String response = authService.login(login, password);
        if (response.equals("logged"))
            return ResponseEntity.ok("Signed in!");
        else
            return ResponseEntity.ok("Wrong login or password!");

    }
    public ResponseEntity<?> getAll(@RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            return ResponseEntity.ok(chatService.getAll());
        }
        return ResponseEntity.ok("Error:Please sign in!");
    }

    public ResponseEntity<?> add(@RequestBody Chat chat,
                                 @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            chatService.add(chat);
            return ResponseEntity.ok("Chat successfully added");
        }
        return ResponseEntity.ok("Error:Please login");
    }

    public ResponseEntity<?> edit(@RequestBody Chat chat,
                                  @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            chatService.update(chat);
            return ResponseEntity.ok(chat);
        }
        return ResponseEntity.ok("Error:Please login");
    }
    public ResponseEntity<?> delete(@RequestBody Chat chat,
                                    @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            chatService.delete(chat);
            return ResponseEntity.ok("Chat successfully deleted");
        }
        return ResponseEntity.ok("Error:Please login");
    }
    public ResponseEntity<?> add(@RequestBody Message message,
                                 @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            message.setMessageType("basic");
            messageService.add(message);
            return ResponseEntity.ok("Message successfully added");
        }
        return ResponseEntity.ok("Error:Please sign in!");
    }
    @PostMapping("/status/{chatId}")
    public ResponseEntity<?> status(@RequestHeader("status") String status,
                                    @RequestHeader("auth") Long userId,
                                    @RequestHeader("auth") String token,
                                    @PathVariable Long chatId) {

                if (authService.isExistByToken(token)) {
                    if ((status.equals("writing") || status.equals("online"))) {
                        Users user = authService.getUserByToken(token);
                        Message message = new Message();
                        message.setUserId(user.getId());
                        message.setChatId(chatId);
                        message.setMessageType(status);
                        if (status.equals("writing")) {
                            message.setText("User writing message");
                            messageService.add(message);
                            return ResponseEntity.ok(message);
                        } else if (status.equals("online")) {
                            message.setText("User is in the chat");
                            messageService.add(message);
                            return ResponseEntity.ok(message);
                        }
                    }
                    return ResponseEntity.ok("Error: bad request");
                }
        return ResponseEntity.ok("Error:Please sign in!");
    }

            }
