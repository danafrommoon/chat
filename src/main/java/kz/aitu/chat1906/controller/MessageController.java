package kz.aitu.chat1906.controller;

import kz.aitu.chat1906.model.Message;
import kz.aitu.chat1906.model.Users;
import kz.aitu.chat1906.repository.MessageRepository;
import kz.aitu.chat1906.service.AuthService;
import kz.aitu.chat1906.service.MessageService;
import kz.aitu.chat1906.service.ParticipantService;
import kz.aitu.chat1906.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/message")
public class MessageController {
    private final MessageService messageService;
    private final ParticipantService participantService;
    private final UserService userService;
    private final AuthService authService;

    @PostMapping
    public ResponseEntity<?> add(@RequestBody Message message,
                                 @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            message.setMessageType("basic");
            messageService.add(message);
            return ResponseEntity.ok("Message successfully added");
        }
        return ResponseEntity.ok("Error:Please login");

    }

    @PostMapping("/status/{chatId}")
    public ResponseEntity<?> status(@RequestHeader("status") String status,
                                    @RequestHeader("auth") String token,
                                    @PathVariable Long chatId) {
        if (authService.isExistByToken(token)) {
            if ((status.equals("writing") || status.equals("online"))) {
                Users user = authService.getUserByToken(token);
                Message message = new Message();
                message.setUserId(user.getId());
                message.setChatId(chatId);
                message.setMessageType(status);
                if (status.equals("writing")) {
                    message.setText("user writing message");
                    messageService.add(message);
                    return ResponseEntity.ok(message);
                } else if (status.equals("online")) {
                    message.setText("user is online");
                    messageService.add(message);
                    return ResponseEntity.ok(message);
                }
            }
            return ResponseEntity.ok("Error: bad request");
        }

        return ResponseEntity.ok("Error:Sign in!");
    }


    @PostMapping("/read/{chatId}")
    public ResponseEntity<?> isRead(@PathVariable Long chatId,
                                    @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            Users user = authService.getUserByToken(token);
            if (participantService.isExistByChatAndUser(chatId, user.getId())) {
                Date date = new Date();
                List<Message> messages = messageService.findNotReadMessages(chatId, user.getId());
                for (Message message : messages) {
                    message.setRead(true);
                    message.setReadTimestamp(date.getTime());
                    messageService.update(message);
                }
                return ResponseEntity.ok("Message read");
            }

            return ResponseEntity.ok("Message read");
        }
        return ResponseEntity.ok("Error: Sign in!");
    }

    @PutMapping
    public ResponseEntity<?> edit(@RequestBody Message message,
                                  @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            messageService.update(message);
            return ResponseEntity.ok(message);
        }
        return ResponseEntity.ok("Error:Sign in!");

    }

    @DeleteMapping
    public ResponseEntity<?> delete(@RequestBody Message message,
                                    @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            messageService.delete(message);
            return ResponseEntity.ok("Message is deleted");
        }
        return ResponseEntity.ok("Error:Sign in!");

    }

    @GetMapping("/messages/{chatId}")
    public ResponseEntity<?> getMessagesByChatId(@PathVariable Long chatId,
                                                 @RequestHeader("auth") Long userId) {
        System.out.println(userId);
        if (participantService.isExistByChatAndUser(chatId, userId)) {
            Date date = new Date();
            List<Message> messages = messageService.findNotDeliveredMessages(chatId, userId);
            for (Message message : messages) {
                message.setDelivered(true);
                message.setDeliveredTimestamp(date.getTime());
                messageService.update(message);
            }
            return ResponseEntity.ok(messageService.getMessagesByChat(chatId));
        } else {
            return ResponseEntity.ok("Error: This user is not in this chat");
        }
    }

    @GetMapping("/messages/notDelivered/{chatId}")
    public ResponseEntity<?> getNotDeliveredByChatId(@PathVariable Long chatId,
                                                     @RequestHeader("auth") String token) {
        if (authService.isExistByToken(token)) {
            Users user = authService.getUserByToken(token);
            if (participantService.isExistByChatAndUser(chatId, user.getId())) {
                Date date = new Date();
                List<Message> messages = messageService.findNotDeliveredMessages(chatId, user.getId());
                for (Message message : messages) {
                    message.setDelivered(true);
                    message.setDeliveredTimestamp(date.getTime());
                    messageService.update(message);
                }
                return ResponseEntity.ok(messageService.getMessagesByChat(chatId));
            } else {
                return ResponseEntity.ok("Error: This user is not in this chat");
            }

        }
        return ResponseEntity.ok("Error: Sign in!");
    }

    @GetMapping("/messages/lastChat/{chatId}")
        public ResponseEntity<?> getLastMessagesByChatId(@PathVariable Long chatId,
                @RequestHeader("auth") String token) {
            if (authService.isExistByToken(token)) {
                Users user = authService.getUserByToken(token);
                if (participantService.isExistByChatAndUser(chatId, user.getId())) {
                    return ResponseEntity.ok(messageService.findLastMessagesByChatId(chatId));
                } else {
                    return ResponseEntity.ok("Error: This user is not in this chat");
                }
            }
            return ResponseEntity.ok("Error:Sign in!");
        }
    @GetMapping("/messages/lastUser/{userId}")
        public ResponseEntity<?> getLastMessagesByUserId(@PathVariable Long userId,
                @RequestHeader("auth") String token) {
            if (authService.isExistByToken(token)) {

                return ResponseEntity.ok(messageService.findLastMessagesByUsertId(userId));
            }
            return ResponseEntity.ok("Error:Sign in!");
        }

    }

