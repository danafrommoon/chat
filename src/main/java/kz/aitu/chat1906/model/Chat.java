package kz.aitu.chat1906.model;

import lombok.Data;

import javax.persistence.*;


@Data
@Entity
@Table(name = "chat")
public class Chat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @JoinColumn(name = "name")
    private String name;


}
